module.exports = {
  // Use the typescript ESLint parser to allow for linting
  // typescript files
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    // eslint-disable-next-line no-undef
    tsconfigRootDir: __dirname, // Support for monorepos
    project: ['./tsconfig.json'],
  },
  env: {
    es2020: true,
    browser: false, // True for something like ReactJS
    node: true,
    jest: true,
  },
  plugins: ['@typescript-eslint', 'jest'],
  extends: [
    'eslint:recommended', // the set of rules which are recommended for all projects by the ESLint Team)
    'plugin:@typescript-eslint/eslint-recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking', // Some highly valuable rules simply require type-checking in order to be implemented correctly
    'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:jest/recommended', // Use eslint recommended rules for jest test files
  ],
  rules: {
    // We're all grownups here, some times you have to ignore
    // the type system and while that should be rarely, its between you and your reviewers.
    '@typescript-eslint/ban-ts-ignore': 'off',
  },
}
