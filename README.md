# eslint-config-ignw

This package provides IGNW's .eslintrc as an extensible shared config for standarized linting of Javascript, NodeJS and Typescript projects.

## How to consume this package in your project

Install [peer dependencies for this package](./package.json) as dev dependencies for your project. Note that installing prettier isn't strictly neccessary but will allow your editor to autoformat your code based off configured eslint rules.

Create an `.eslintrc` or `.eslintrc.js` file in your project and inherit this packages configs using the `extends` property:

```javascript
{
    "extends": "eslint-config-ignw"
}
```

If you only want a specific subset of linter rules you can do so like:

```javascript
 "extends": [
    "eslint-config-ignw/react",
  ],
```

Finally add an npm run script for running the linter and automatically fixing auto fixable issues

```json
"scripts": {
    "lint": "eslint . --ext .js,.jsx,.ts,.tsx",
    "lint:fix": "eslint . --ext .js,.jsx,.ts,.tsx . --fix"
  },
```

## How to develop this package

## Development

Install package development dependencies

```bash
npm install
```

Update the rules in `base.js` or add another subset of rules by creating a file (e.g. `react.js`) and extending that in `index.js` to export those new rules by default.

## Testing / Self Linting

When developing you can lint the package itself by linking and consuming the local version of the package and then running the linter command for the package:

```bash
npm link
npm link eslint-config-ignw
npm run lint:fix
```

Similary to test out the changes to the linting package from another package, first run

```bash
npm link
```

from the `eslint-config-ignw` directory, and then in the consuming package

```bash
npm link eslint-config-ignw
```

Finally run the consuming packages lint command to test out your changes.

## Publishing

Checkout a branch, code and commit changes, get code reviewed and approved.

Once the code is approved update the packages version (using semver style) by running

```bash
npm version X.Y.Z
```

Log into to your npm account

```bash
npm login
```

Then publish the package to npm for public consumpation:

```bash
npm publish
```

## How this package works

The folks at ESLint were kind enough to provide developer docs for [shared eslint configs](https://eslint.org/docs/developer-guide/shareable-configs).

For an example of a mature eslint config see the [AirBnB eslint config package](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb).
